<?php 
include "herolist.php";
$gameplayFocus = $_GET['type'];
?>

<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title>Hero Lore</title>
    <link rel="stylesheet" href="css/focus.css" type="text/css">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton:300,400' rel='stylesheet' type='text/css'>
 </head>
 <body>
 <header>
 	<h1>- 

 	<?php
 	switch ($gameplayFocus) {
 		case 'lasthits':
 			echo "Last Hits";
 			break;
 		case 'denies':
 			echo "Denies";
 			break;
 		case 'mechanics':
 			echo "Mechanics";
 			break;
 		
 		default:
 			# code...
 			break;
 	}
 	?>

 	-</h1>
 	<h2>

 	</h2>
 </header>
 <div class="youtube">
 <?php
 	switch ($gameplayFocus) {
 		case 'lasthits':
 			echo '<div class="youtube"><iframe width="640" height="360" src="//www.youtube.com/embed/pdzNEl7xZAM?rel=0" frameborder="0" allowfullscreen></iframe></div>';
 			break;
 		case 'denies':
 			echo '<object width="640" height="360"><param name="movie" value="//www.youtube.com/v/pdzNEl7xZAM?hl=en_US&amp;version=3&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/pdzNEl7xZAM?hl=en_US&amp;version=3&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>';
 			break;
 		case 'mechanics':
 			echo "Mechanics";
 			break;
 		
 		default:
 			# code...
 			break;
 	}
 	?>
 	</div>
 </body>
</html>