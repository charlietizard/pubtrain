<?php 
include "herolist.php";
$heroName = $_GET['name'];
?>

<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title>Hero Lore</title>
    <link rel="stylesheet" href="css/lore.css" type="text/css">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton:300,400' rel='stylesheet' type='text/css'>
 </head>
 <body>
 	<?php
 	switch ($heroName) {
 		case 'anti-mage':
 			echo "<header>";
 			echo "<h1>- Magina -</h1>";
 			echo "<h2>The Anti-Mage</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The monks of Turstarkuri watched the rugged valleys below their mountain monastery as wave after wave of invaders swept through the lower kingdoms. Ascetic and pragmatic, in their remote monastic eyrie they remained aloof from mundane strife, wrapped in meditation that knew no gods or elements of magic. Then came the Legion of the Dead God, crusaders with a sinister mandate to replace all local worship with their Unliving Lord's poisonous nihilosophy. From a landscape that had known nothing but blood and battle for a thousand years, they tore the souls and bones of countless fallen legions and pitched them against Turstarkuri. The monastery stood scarcely a fortnight against the assault, and the few monks who bothered to surface from their meditations believed the invaders were but demonic visions sent to distract them from meditation. They died where they sat on their silken cushions. Only one youth survived--a pilgrim who had come as an acolyte, seeking wisdom, but had yet to be admitted to the monastery. He watched in horror as the monks to whom he had served tea and nettles were first slaughtered, then raised to join the ranks of the Dead God's priesthood. With nothing but a few of Turstarkuri's prized dogmatic scrolls, he crept away to the comparative safety of other lands, swearing to obliterate not only the Dead God's magic users--but to put an end to magic altogether.";
 			echo "</p>";
 			break;
 		case 'axe':
 			echo "<header>";
 			echo "<h1>- Mogul Khan -</h1>";
 			echo "<h2>The Axe</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "As a grunt in the Army of Red Mist, Mogul Khan set his sights on the rank of Red Mist General. In battle after battle he proved his worth through gory deed. His rise through the ranks was helped by the fact that he never hesitated to decapitate a superior. Through the seven year Campaign of the Thousand Tarns, he distinguished himself in glorious carnage, his star of fame shining ever brighter, while the number of comrades in arms steadily dwindled. On the night of ultimate victory, Mogul declared himself the new Red Mist General, and took on the ultimate title of 'Axe.' But his troops now numbered zero. Of course, many had died in battle, but a significant number had also fallen to Axe's blade. Needless to say, most soldiers now shun his leadership. But this matters not a whit to Axe, who knows that a one-man army is by far the best.";
 			echo "</p>";
 			break;	
 		case 'bane':
 			echo "<header>";
 			echo "<h1>- Atropos -</h1>";
 			echo "<h2>The Bane Elemental</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "When the gods have nightmares, it is Bane Elemental who brings them. Also known as Atropos, Bane was born from the midnight terrors of the goddess Nyctasha. A force of terror too powerful to be contained by sleep, he surfaced from her slumbers, fed upon her immortality, and stole his vaporous form from her inky blood. He is the essence of fear. Mortals who hear his voice hear their darkest secrets whispered in their ear. He calls to the hidden fear in every Hero's heart. Wakefulness is no protection, for Bane's black blood, continuously dripping, is a tar that traps his enemies in nightmare. In the presence of Bane, every Hero remembers to fear the dark.";
 			echo "</p>";
 			break;
 		case 'bloodseeker':
 			echo "<header>";
 			echo "<h1>- Strygwyr -</h1>";
 			echo "<h2>The Bloodseeker</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Strygwyr the Bloodseeker is a ritually sanctioned hunter, Hound of the Flayed Twins, sent down from the mist-shrouded peaks of Xhacatocatl in search of blood. The Flayed Ones require oceanic amounts of blood to keep them sated and placated, and would soon drain their mountain empire of its populace if the priests of the high plateaus did not appease them. Strygwyr therefore goes out in search of carnage. The vital energy of any blood he lets, flows immediately to the Twins through the sacred markings on his weapons and armor. Over the years, he has come to embody the energy of a vicious hound; in battle he is savage as a jackal. Beneath the Mask of the Bloodseeker, in the rush of bloody quenching, it is said that you can sometime see the features of the Flayers taking direct possession of their Hound.";
 			echo "</p>";
 			break;
 		case 'crystalmaiden':
 			echo "<header>";
 			echo "<h1>- Rylai -</h1>";
 			echo "<h2>The Crystal Maiden</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Born in a temperate realm, raised with her fiery older sister Lina, Rylai the Crystal Maiden soon found that her innate elemental affinity to ice created trouble for all those around her. Wellsprings and mountain rivers froze in moments if she stopped to rest nearby; ripening crops were bitten by frost, and fruiting orchards turned to mazes of ice and came crashing down, spoiled. When their exasperated parents packed Lina off to the equator, Rylai found herself banished to the cold northern realm of Icewrack, where she was taken in by an Ice Wizard who had carved himself a hermitage at the crown of the Blueheart Glacier. After long study, the wizard pronounced her ready for solitary practice and left her to take his place, descending into the glacier to hibernate for a thousand years. Her mastery of the Frozen Arts has only deepened since that time, and now her skills are unmatched.";
 			echo "</p>";
 			break;
 		case 'drowranger':
 			echo "<header>";
 			echo "<h1>- Traxex -</h1>";
 			echo "<h2>The Drow Ranger</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Drow Ranger's given name is Traxex—a name well suited to the short, trollish, rather repulsive Drow people. But Traxex herself is not a Drow. Her parents were travelers in a caravan set upon by bandits, whose noisy slaughter of innocents roused the ire of the quiet Drow people. After the battle settled, the Drow discovered a small girl-child hiding in the ruined wagons, and agreed she could not be abandoned. Even as child, Traxex showed herself naturally adept at the arts they prized: Stealth, silence, subtlety. In spirit, if not in physique, she might have been a Drow changeling, returned to her proper home. But as she grew, she towered above her family and came to think of herself as ugly. After all, her features were smooth and symmetrical, entirely devoid of warts and coarse whiskers. Estranged from her adopted tribe, she withdrew to live alone in the woods. Lost travelers who find their way from the forest sometimes speak of an impossibly beautiful Ranger who peered at them from deep among the trees, then vanished like a dream before they could approach. Lithe and stealthy, icy hot, she moves like mist in silence. That whispering you hear is her frozen arrows finding an enemy's heart.";
 			echo "</p>";
 			break;
 		case 'earthshaker':
 			echo "<header>";
 			echo "<h1>- Raigor Stonehoof -</h1>";
 			echo "<h2>The Earthshaker</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Drow Ranger's given name is Traxex—a name well suited to the short, trollish, rather repulsive Drow people. But Traxex herself is not a Drow. Her parents were travelers in a caravan set upon by bandits, whose noisy slaughter of innocents roused the ire of the quiet Drow people. After the battle settled, the Drow discovered a small girl-child hiding in the ruined wagons, and agreed she could not be abandoned. Even as child, Traxex showed herself naturally adept at the arts they prized: Stealth, silence, subtlety. In spirit, if not in physique, she might have been a Drow changeling, returned to her proper home. But as she grew, she towered above her family and came to think of herself as ugly. After all, her features were smooth and symmetrical, entirely devoid of warts and coarse whiskers. Estranged from her adopted tribe, she withdrew to live alone in the woods. Lost travelers who find their way from the forest sometimes speak of an impossibly beautiful Ranger who peered at them from deep among the trees, then vanished like a dream before they could approach. Lithe and stealthy, icy hot, she moves like mist in silence. That whispering you hear is her frozen arrows finding an enemy's heart.";
 			echo "</p>";
 			break;
 		case 'juggernaut':
 			echo "<header>";
 			echo "<h1>- Yurnero -</h1>";
 			echo "<h2>The Juggernaut</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "No one has ever seen the face hidden beneath the mask of Yurnero the Juggernaut. It is only speculation that he even has one. For defying a corrupt lord, Yurnero was exiled from the ancient Isle of Masks--a punishment that saved his life. The isle soon after vanished beneath the waves in a night of vengeful magic. He alone remains to carry on the Isle's long Juggernaut tradition, one of ritual and swordplay. The last practitioner of the art, Yurnero's confidence and courage are the result of endless practice; his inventive bladework proves that he has never stopped challenging himself. Still, his motives are as unreadable as his expression. For a hero who has lost everything twice over, he fights as if victory is a foregone conclusion.";
 			echo "</p>";
 			break;
 		case 'mirana':
 			echo "<header>";
 			echo "<h1>- Mirana -</h1>";
 			echo "<h2>The Princess of the Moon</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Born to a royal family, a blood princess next in line for the Solar Throne, Mirana willingly surrendered any claim to mundane land or titles when she dedicated herself completely to the service of Selemene, Goddess of the Moon. Known ever since as Princess of the Moon, Mirana prowls the sacred Nightsilver Woods searching for any who would dare poach the sacred luminous lotus from the silvery pools of the Goddess's preserve. Riding on her enormous feline familiar, she is poised, proud and fearless, attuned to the phases of the moon and the wheeling of the greater constellations. Her bow, tipped with sharp shards of lunar ore, draws on the moon's power to charge its arrows of light.";
 			echo "</p>";
 			break;
 		case 'morphling':
 			echo "<header>";
 			echo "<h1>- Morphling -</h1>";
 			echo "<h2>The Morphling</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "For dark eons the comet circled. Held in thrall to a distant sun, bound by gravity’s inexorable pull, the massive ball of ice careened through the blackness between worlds, made strange by its dark journey. On the eve of the ancient war of the Vloy, it punched down through the sky and lit a glowing trail across the night, a sign both armies took for an omen. The frozen ball melted in a flash of boiling heat, as below two forces enjoined in battle across the border of a narrow river. Thus freed from its icy stasis, the Morphling was born into conflict, an elemental power at one with the tides of the ocean, capricious and unconstrained. He entered the fight, instinctively taking the form of the first general who dared set foot across the water, and then struck him dead. As the motley warriors clashed, he shifted from form to form throughout the battle, instantly absorbing the ways of these strange creatures--now a footsoldier, now an archer, now the cavalryman--until, by the time the last soldier fell, Morphling had played every part. The battle's end was his beginning.";
 			echo "</p>";
 			break;
 		case 'shadowfiend':
 			echo "<header>";
 			echo "<h1>- Nevermore -</h1>";
 			echo "<h2>The Shadow Fiend</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Shadow Fiend has the soul of a poet, and in fact he has thousands of them. Over the ages he has claimed the souls of poets, priests, emperors, beggars, slaves, philosophers, criminals and (naturally) heroes; no sort of soul escapes him. What he does with them is unknown. No one has ever peered into the Abysm whence Shadow Fiend reaches out like an eel from among astral rocks. Does he devour them one after another? Does he mount them along the halls of an eldritch temple, or pickle the souls in necromantic brine? Is he merely a puppet, pushed through the dimensional rift by a demonic puppeteer? Such is his evil, so intense his aura of darkness, that no rational mind may penetrate it. Of course, if you really want to know where the stolen souls go, there's one sure way to find out: Add your soul to his collection. Or just wait for Nevermore.";
 			echo "</p>";
 			break;
 		case 'phantomlancer':
 			echo "<header>";
 			echo "<h1>- Azwraith -</h1>";
 			echo "<h2>The Phantom Lancer</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The remote village of Pole had no knowledge of the wars raging in the heart of the kingdom. For them, the quiet of spear fishing, and a family meal were all that a full life required. Yet war came for them nonetheless. Joining the able-bodied conscripts as they filed past their homes, the humble lancer Azwraith vowed to bring peace to his kingdom, and in so doing, his people. Placed with his kin in the vanguard of the final assault against the Dread Magus Vorn, the cost to his fellows was absolute. As the charging force battled toward the fortress, Azwraith alone among his kind remained standing, and he alone was able to infiltrate the keep. Focused and infuriated by the slaughter of his brothers, Azwraith bested each of the wizard's deadly traps and conjured guardians. Soon the simple fisherman arrived at Vorn's tower sanctum. The pair dueled through the night, pike to staff, as chaos raged below, and with a deafening cry Azwraith pierced his enemy. But the wizard did not simply expire; he exploded into uncountable shards of light, penetrating his killer with power. As the dust settled and the smoke of combat began to clear, Azwraith found himself standing among a throng of his kin. Each seemed to be dressed as he was, each seemed armed as he was, and he could sense that each thought as he did. Aware that his allies were approaching, he willed these phantoms to hide themselves, and one by one they began to vanish into nothingness. As the soldiers came upon the sanctum, they found the warrior that had bested the wizard. When they approached their champion, the lancer vanished. The pikeman who had stood before them was no more than another phantom.";
 			echo "</p>";
 			break;
 		case 'puck':
 			echo "<header>";
 			echo "<h1>- Puck -</h1>";
 			echo "<h2>The Faerie Dragon</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "While Puck seems at first glance a mischievous, childish character, this quality masks an alien personality. The juvenile form of a Faerie Dragon, a creature that lives for eons, Puck spends countless millennia in its childish form. So while it is technically true that Puck is juvenile, it will continue to be so when the cities of the present age have sloughed away into dust. Its motives are therefore inscrutable, and what appears to be play may in fact indicate a darker purpose or just its endless fondness for mischief.
";
 			echo "</p>";
 			break;
 		case 'pudge':
 			echo "<header>";
 			echo "<h1>- Pudge -</h1>";
 			echo "<h2>The Butcher</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In the Fields of Endless Carnage, far to the south of Quoidge, a corpulent figure works tirelessly through the night--dismembering, disembowelling, piling up the limbs and viscera of the fallen that the battlefield might be clear by dawn. In this cursed realm, nothing can decay or decompose; no corpse may ever return to the earth from which it sprang, no matter how deep you dig the grave. Flocked by carrion birds who need him to cut their meals into beak-sized chunks, Pudge the Butcher hones his skills with blades that grow sharper the longer he uses them. Swish, swish, thunk. Flesh falls from the bone; tendons and ligaments part like wet paper. And while he always had a taste for the butchery, over the ages, Pudge has developed a taste for its byproduct as well. Starting with a gobbet of muscle here, a sip of blood there...before long he was thrusting his jaws deep into the toughest of torsos, like a dog gnawing at rags. Even those who are beyond fearing the Reaper, fear the Butcher.";
 			echo "</p>";
 			break;
 		case 'razor':
 			echo "<header>";
 			echo "<h1>- Razor -</h1>";
 			echo "<h2>The Lightning Revenant</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Among the emblematic powers that populate the Underscape, Razor the Lightning Revenant is one of the most feared. With his whip of lightning, he patrols the Narrow Maze, that treacherous webwork of passages by which the souls of the dead are sorted according to their own innate intelligence, cunning and persistence. Drifting above the Maze, Razor looks down on the baffled souls below, and delivers jolts of scalding electricity that both punish and quicken the souls as they decide their own fates, hurrying on toward luminous exits or endlessly dark pits. Razor is the eternal embodiment of a dominating power, abstract and almost clinical in his application of power. Yet he has a lordly air that suggests he takes a sardonic satisfaction in his work.";
 			echo "</p>";
 			break;
 		case 'sandking':
 			echo "<header>";
 			echo "<h1>- Crixalis -</h1>";
 			echo "<h2>The Sand King</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The sands of the Scintillant Waste are alive and sentient--the whole vast desert speaks to itself, thinking thoughts only such a vastness can conceive. But when it needs must find a form to communicate with those of more limited scope, it frees a fragment of itself, and fills a carapace of magic armor formed by the cunning Djinn of Qaldin. This essential identity calls itself Crixalis, meaning 'Soul of the Sand,' but others know it as Sand King. Sand King takes the form of a huge arachnid, inspired by the Scintillant Waste's small but ubiquitous denizens; and this is a true outward expression of his ferocious nature. Guardian, warrior, ambassador--Sand King is all of these things, inseparable from the endless desert that gave him life.";
 			echo "</p>";
 			break;
 		case 'stormspirit':
 			echo "<header>";
 			echo "<h1>- Raijin Thunderkeg -</h1>";
 			echo "<h2>The Storm Spirit</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Storm Spirit is literally a force of nature--the wild power of wind and weather, bottled in human form. And a boisterous, jovial, irrepressible form it is! As jolly as a favorite uncle, he injects every scene with crackling energy. But it was not always thus, and there was tragedy in his creation. Generations ago, in the plains beyond the Wailing Mountains, a good people lay starving in drought and famine. A simple elementalist, Thunderkeg by name, used a forbidden spell to summon the spirit of the storm, asking for rain. Enraged at this mortal’s presumption, the Storm Celestial known as Raijin lay waste to the land, scouring it bare with winds and flood. Thunderkeg was no match for the Celestial--at least until he cast a suicidal spell that forged their fates into one: he captured the Celestial in the cage of his own body. Trapped together, Thunderkeg's boundless good humor fused with Raijin's crazed energy, creating the jovial Raijin Thunderkeg, a Celestial who walks the world in physical form.";
 			echo "</p>";
 			break;
 		case 'sven':
 			echo "<header>";
 			echo "<h1>- Sven -</h1>";
 			echo "<h2>The Rogue Knight</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Sven is the bastard son of a Vigil Knight, born of a Pallid Meranth, raised in the Shadeshore Ruins. With his father executed for violating the Vigil Codex, and his mother shunned by her wild race, Sven believes that honor is to be found in no social order, but only in himself. After tending his mother through a lingering death, he offered himself as a novice to the Vigil Knights, never revealing his identity. For thirteen years he studied in his father's school, mastering the rigid code that declared his existence an abomination. Then, on the day that should have been his In-Swearing, he seized the Outcast Blade, shattered the Sacred Helm, and burned the Codex in the Vigil's Holy Flame. He strode from Vigil Keep, forever solitary, following his private code to the last strict rune. Still a knight, yes...but a Rogue Knight. He answers only to himself.";
 			echo "</p>";
 			break;
 		case 'tiny':
 			echo "<header>";
 			echo "<h1>- Tiny -</h1>";
 			echo "<h2>The Stone Giant</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Coming to life as a chunk of stone, Tiny's origins are a mystery on which he continually speculates. He is a Stone Giant now, but what did he use to be? A splinter broken from a Golem's heel? A shard swept from a gargoyle-sculptor's workshop? A fragment of the Oracular Visage of Garthos? A deep curiosity drives him, and he travels the world tirelessly seeking his origins, his parentage, his people. As he roams, he gathers weight and size; the forces that weather lesser rocks, instead cause Tiny to grow and ever grow.";
 			echo "</p>";
 			break;
 		case 'vengefulspirit':
 			echo "<header>";
 			echo "<h1>- Shendelzare -</h1>";
 			echo "<h2>The Vengeful Spirit</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Even the most contented Skywrath is an ill-tempered creature, naturally inclined to seek revenge for the slightest insult. But Vengeful Spirit is the essence of vengeance. Once a proud and savage Skywrath scion, Shendelzare was first in succession for the Ghastly Eyrie until a sister's treachery robbed her of her birthright. Snared in an assassin's net, Shendelzare tore free only at the cost of her wings, limping away in the ultimate humiliation: On foot. With her wings broken, she knew the Skywrath would never accept her as ruler; and in the highest roost of the Eyrie, inaccessible except by winged flight, her sister was untouchable. Unwilling to live as a flightless cripple, and desiring revenge far more than earthly power, the fallen princess drove a bargain with the goddess Scree'auk: She surrendered her broken body for an imperishable form of spirit energy, driven by vengeance, capable of doing great damage in the material plane. She may spend eternity flightless, but she will have her revenge.";
 			echo "</p>";
 			break;
 		case 'windranger':
 			echo "<header>";
 			echo "<h1>- Lyralei -</h1>";
 			echo "<h2>The Windranger</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The western forests guard their secrets well. One of these is Lyralei, master archer of the wood, and favored godchild of the wind. Known now as Windranger, Lyralei's family was killed in a storm on the night of her birth—-their house blown down by the gale, contents scattered to the winds. Only the newborn survived among the debris field of death and destruction. In the quiet after the storm, the wind itself took notice of the lucky infant crying in the grass. The wind pitied the child and so lifted her into the sky and deposited her on a doorstep in a neighboring village. In the years that followed, the wind returned occasionally to the child’s life, watching from a distance while she honed her skills. Now, after many years of training, Windranger fires her arrows true to their targets. She moves with blinding speed, as if hastened by a wind ever at her back. With a flurry of arrows, she slaughters her enemies, having become, nearly, a force of nature herself.";
 			echo "</p>";
 			break;
 		case 'zeus':
 			echo "<header>";
 			echo "<h1>- Zeus -</h1>";
 			echo "<h2>The Lord of Heaven</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Lord of Heaven, father of gods, Zeus treats all the Heroes as if they are his rambunctious, rebellious children. After being caught unnumbered times in the midst of trysts with countless mortal women, his divine wife finally gave him an ultimatum: 'If you love mortals so much, go and become one. If you can prove yourself faithful, then return to me as my immortal husband. Otherwise, go and die among your creatures.' Zeus found her logic (and her magic) irrefutable, and agreed to her plan. He has been on his best behavior ever since, being somewhat fonder of immortality than he is of mortals. But to prove himself worthy of his eternal spouse, he must continue to pursue victory on the field of battle.";
 			echo "</p>";
 			break;
 		case 'kunkka':
 			echo "<header>";
 			echo "<h1>- Kunkka -</h1>";
 			echo "<h2>The Admiral</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "As Admiral of the mighty Claddish Navy, Kunkka was charged with protecting the isles of his homeland when the Demons of the Cataract made a concerted grab at the lands of men. After years of small sorties, and increasingly bold and devastating attacks, the Demon Fleet flung all its carnivorous ships at the Trembling Isle. Desperate, the Suicide-Mages of Cladd committed their ultimate rite, summoning a host of ancestral spirits to protect the fleet. Against the Demons, this was just barely enough to turn the tide. As Kunkka watched the Demons take his ships down one by one, he had the satisfaction of wearing away their fleet with his ancestral magic. But at the battle's peak, something in the clash of demons, men and atavistic spirits must have stirred a fourth power that had been slumbering in the depths. The waves rose up in towering spouts around the few remaining ships, and Maelrawn the Tentacular appeared amid the fray. His tendrils wove among the ships, drawing demon and human craft together, churning the water and wind into a raging chaos. What happened in the crucible of that storm, none may truly say. The Cataract roars off into the void, deserted by its former denizens. Kunkka is now Admiral of but one ship, a ghostly rig which endlessly replays the final seconds of its destruction. Whether he died in that crash is anyone's guess. Not even Tidehunter, who summoned Maelrawn, knows for sure.";
 			echo "</p>";
 			break;
 		case 'lina':
 			echo "<header>";
 			echo "<h1>- Lina -</h1>";
 			echo "<h2>The Slayer</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The sibling rivalries between Lina the Slayer, and her younger sister Rylai, the Crystal Maiden, were the stuff of legend in the temperate region where they spent their quarrelsome childhoods together. Lina always had the advantage, however, for while Crystal was guileless and naive, Lina's fiery ardor was tempered by cleverness and conniving. The exasperated parents of these incompatible offspring went through half a dozen homesteads, losing one to fire, the next to ice, before they realized life would be simpler if the children were separated. As the oldest, Lina was sent far south to live with a patient aunt in the blazing Desert of Misrule, a climate that proved more than comfortable for the fiery Slayer. Her arrival made quite an impression on the somnolent locals, and more than one would-be suitor scorched his fingers or went away with singed eyebrows, his advances spurned. Lina is proud and confident, and nothing can dampen her flame.";
 			echo "</p>";
 			break;
 		case 'lich':
 			echo "<header>";
 			echo "<h1>- Ethreain -</h1>";
 			echo "<h2>The Lich</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In life, the frost-mage Ethreain (not yet a Lich) had used the threat of destructive ice to enslave entire kingdoms. His subjects, aided by a few desperate magicians, eventually grew bold enough to ambush him. Armed with enough charmed rope to bind him forever, they tied the frost mage to adamant weights and dropped him in a pool known chiefly for being bottomless. It wasn't. He only fell for a year or so before an outcrop snagged him. There he rested, dead but undecaying, until the geomancer Anhil thought to verify the legend of the supposedly bottomless Black Pool. Anhil's plumbline snarled with the ropes that bound the drowned magician, and up he hauled an unexpected prize. Thinking that by rendering the dead undead, he could question the Lich about the properties of the pool, he removed the bindings and commenced a simple rite of resurrection. Even the descendants of Ethreain's enemies were long forgotten by time, so there were none to warn Anhil against imprudence. But he learned the error of his judgment almost immediately, as Lich threw off the shackles and consumed him.";
 			echo "</p>";
 			break;
 		case 'lion':
 			echo "<header>";
 			echo "<h1>- Lion -</h1>";
 			echo "<h2>The Demon Witch</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Once a Grandmaster of the Demon Witch tradition of sorcery, Lion earned fame among his brethren for fighting on the side of light and righteousness. But adulation corrupts. With powers surpassed only by his ambition, the mage was seduced by a demon and turned to evil, trading his soul for prestige. After committing horrible crimes that marred his soul, he was abandoned. The demon betrayed him, striking better deals with his enemies. Such was Lion’s rage that he followed the demon back to hell and slew it, ripping it limb from limb, taking its demonic hand for his own. However, such demonoplasty comes at a cost. Lion was transfigured by the process, his body transformed into something unrecognizable. He rose from hell, rage incarnate, slaying even those who had once called him master, and laying waste to the lands where he had once been so adored. He survives now as the sole practitioner of the Demon Witch tradition, and those who present themselves as acolytes or students are soon relieved of their mana and carried off by the faintest gust of wind.";
 			echo "</p>";
 			break;
 		case 'shadowshaman':
 			echo "<header>";
 			echo "<h1>- Rhasta -</h1>";
 			echo "<h2>The Shadow Shaman</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Born in the Bleeding Hills, Rhasta was just a starving youngling when picked up by a travelling con-man. For two pins of copper, the old con-man would tell your fortune. For three, he’d castrate your pig, for five, he’d circumcise your sons. For a good meal, he’d don his shaman garb, read from his ancient books, and lay a curse upon your enemies. His strange new youngling, part hill troll, part…something else, worked as assistant and lent an air of the exotic to the con-man’s trade. Always one step ahead of cheated customers, one town ahead of a pursuing patronage, the two trekked across the blighted lands until one day the con-man realized that the little youngling could actually do what he only pretended at. His ward had a gift—a gift that customers valued. And so the youngling Rhasta was thrust before the crowds, and the trade-name Shadow Shaman was born. The two continued from town to town, conjuring for money as Shadow Shaman’s reputation grew. Eventually, the pair’s duplicitous past caught up with them, and they were ambushed by a mob of swindled ex-clients. The con-man was slain, and for the first time, Rhasta used his powers for darkness, massacring the attackers. He buried his beloved master, and now uses his powers to destroy any who would seek to do him harm.";
 			echo "</p>";
 			break;
 		case 'slardar':
 			echo "<header>";
 			echo "<h1>- Slardar -</h1>";
 			echo "<h2>The Slithereen Guard</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Slardar is a Slithereen, one of the Deep Ones, guardian of the great wealth of sunken cities and the ancient riches buried there. In the lightless gulf of the great ocean abysses, the Slithereen Guard carries his lure-light with him through the secret treasure rooms. Subaqueous thieves (sent into the deeps by covetous dryland sorcerers) are drawn in by its friendly glow, never to return. He is utterly loyal, and his taciturn nature hides deep knowledge of the most secret places of the sea. He rises to the shallows in spite of the pain caused him by brightness, to commit reconnaissance, to make sure no one is conspiring against the depths, and sometimes in relentless pursuit of the rare few who manage to steal off with an item from the Sunken Treasury. Because he has spent his whole life at great pressure, under tremendous weight of the sea, Slardar the Slithereen Guard is a creature of great power.";
 			echo "</p>";
 			break;
 		case 'tidehunter':
 			echo "<header>";
 			echo "<h1>- Leviathan -</h1>";
 			echo "<h2>The Tidehunter</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The Tidehunter known as Leviathan was once the champion of the Sunken Isles, but his motives are as mysterious as those of his people. We all know the importance of the Drylanders' shipping lanes, how empires may rise and fall according to who controls the open water. Far less is known of the submarine lanes, and how the warring tribes of the Meranthic Diaspora have carved out habitations through endless undersea skirmishes. In the fragile treaties between the Mer and Men, we can glimpse the extent of the drowned empires, but their politics appear complex and opaque. It would seem that Leviathan tired of such petty strife, and set off on his own, loyal only to his abyssal god, Maelrawn the Tentacular. He stalks the shallows now in search of men or meranths who stray into his path, and with a particular loathing for Admiral Kunkka, who has long been his nemesis for reasons lost in the deepest trenches of the sea.";
 			echo "</p>";
 			break;
 		case 'witchdoctor':
 			echo "<header>";
 			echo "<h1>- Zharvakko -</h1>";
 			echo "<h2>The Witch Doctor</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "A wiry silhouette hitches forward—uneven of feature and limb, bizarre of gait, relentlessly criss-crossing the battlefield in search of that vital weak point where his talents can do most good, and most harm. Whether broken or mismade it is not clear, but still, none can doubt the power carried in his twisted physique. A long staff thumps the earth as the Witch Doctor advances, deploying a terrifying arsenal of fetishes, hexes and spells. It is a body of magical knowledge learned over several lifetimes in the island highlands of Prefectura, now wielded with precise accuracy against his enemies. Witch Doctor is your best friend, or your worst enemy--healing allies and laying waste to all who oppose him.";
 			echo "</p>";
 			break;
 		case 'riki':
 			echo "<header>";
 			echo "<h1>- Riki -</h1>";
 			echo "<h2>The Stealth Assassin</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Riki was born middle child to the great dynasty of Tahlin. With an older brother groomed for the throne, and a younger brother coddled and kept, Riki, the small middle son, seemed born for the art of invisibility. It was an art he cultivated, and one which ultimately saved his life on the night that his people were betrayed and his family slaughtered. Of all the royal line, he alone escaped—small and agile, unassuming, using smoke as cover. He cut his way out of the royal grounds, using the advantage of surprise, quietly slitting the throats of one enemy warrior after another. Now free of his royal responsibilities, Riki uses his talents in service to a new trade: Stealth Assassin. He silences his enemies, sharpening his skills, hoping to one day take revenge on those who killed his family and robbed him of his birthright.";
 			echo "</p>";
 			break;
 		case 'enigma':
 			echo "<header>";
 			echo "<h1>- Enigma -</h1>";
 			echo "<h2>The Enigma</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Nothing is known of Enigma’s background. There are only stories and legends, most of them apocryphal, passed down through the ages. In truth, Enigma is a mystery for whom the only true biography is description: he is a universal force, a consumer of worlds. He is a being of the void, at times corporeal, other times ethereal. A beast between the planes. There are stories that say he was once a great alchemist who tried to unlock the secrets of the universe and was cursed for his arrogance. Other legends tell that he is an ancient being of strange gravity, the abyss personified—a twisted voice from out the original darkness, before the first light in the universe. And there are older legends that say he is the first collapsed star, a black hole grown complicated and sentient—his motivations unknowable, his power inexorable, a force of destruction unleashed upon existence itself.";
 			echo "</p>";
 			break;
 		case 'tinker':
 			echo "<header>";
 			echo "<h1>- Boush -</h1>";
 			echo "<h2>The Tinker</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Boush the Tinker's diminutive race is known for its intelligence, its cunning, and its prickly relationship with magic. As a matter of pride, they survive by their wits, and use only those powers of nature that may be unlocked through rational methodologies. Even this forbearance has led to a great deal of trouble, as Boush can attest. Once a key investigator of natural law, Boush the Tinker led a vast intellectual investigation into the workings of nature, founding a subterranean laboratory in the rumored, mist-wreathed wastes of the Violet Plateau. While scorning mages for the dangers they visit upon the world, Boush and his Tinker associates haughtily wrenched open a portal to some realm beyond comprehension and ushered in some nightmares of their own. A black mist rose from the cavernous interior of the Violet Plateau, shrouding it in permanent darkness from which sounds of horror perpetually emanate. Boush escaped with only his wits and the contraptions he carried, the sole Tinker to survive the Violet Plateau Incident.";
 			echo "</p>";
 			break;
 		case 'sniper':
 			echo "<header>";
 			echo "<h1>- Kardel Sharpeye -</h1>";
 			echo "<h2>The Sniper</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Kardel Sharpeye was born deep in the mountainous valleys of Knollen where, since time immemorial, the folk have survived by hunting the strange, cliff-dwelling steepstalkers above their village—killing them from a distance and collecting their carcasses where they fell. Sharpeye was among the best of these strange folk for whom projectile weapons are but another appendage, and to shoot is as natural as to touch. On his day of summoning, when he was to gain full standing in his village, Sharpeye took the ancient test: a single shot from the valley floor to strike a beast down from the cliffs. To miss was to be dishonored. With his entire village standing vigil, Sharpeye took his shot. A steepstalker fell; the crowd cheered. But when the carcass was collected, the village grew silent, for the elders found that the bullet had pierced its glittering central eye then fallen to be clenched in the steepstalker's mandibles. This ominous sign was the literal opening of a dark prophecy, foretelling both greatness and exile for the gunman who made such a shot. Sharpeye the Sniper was thus, by his own skill, condemned to make his way apart from his people—and unwelcome back among them until he has fulfilled the remainder of the prophecy by attaining legendary stature on a field of battle.";
 			echo "</p>";
 			break;
 		case 'necrophos':
 			echo "<header>";
 			echo "<h1>- Rotund'jere -</h1>";
 			echo "<h2>The Necrophos</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In a time of great plague, an obscure monk of dark inclinations, one Rotund'jere, found himself promoted to the rank of Cardinal by the swift death of all his superiors. While others of the order went out to succor the ill, the newly ordained cardinal secluded himself within the Cathedral of Rumusque, busily scheming to acquire the property of dying nobles, promising them spiritual rewards if they signed over their terrestrial domains. As the plague receded to a few stubborn pockets, his behavior came to the attention of the greater order, which found him guilty of heresy and sentenced him to serve in the plague ward, ensorcelled with spells that would ensure him a slow and lingering illness. But they had not counted on his natural immunity. Rotund'jere caught the pox, but instead of dying, found it feeding his power, transforming him into a veritable plague-mage, a Pope of Pestilence. Proclaiming himself the Necrophos, he travels the world, spreading plague wherever he goes, and growing in terrible power with every village his pestilential presence obliterates.";
 			echo "</p>";
 			break;
 		case 'warlock':
 			echo "<header>";
 			echo "<h1>- Demnok Lannik -</h1>";
 			echo "<h2>The Warlock</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "As Chief Curator and Head of Acquisitions for the Arcane Archives of the Ultimyr Academy, Demnok Lannik was tireless in his pursuit of lost, rare and forbidden tomes. No cursed temple was so foreboding, no cavern path so treacherous, that any concern for his own survival could dissuade him from entering if rumors hinted that some pamphlet of primordial lore might still survive in its depths. However, so often did his investigations trigger the wrath of protector entities, that he finally found it necessary to master the arts of magic. He bent himself to learning sorcery with the same thorough obsessiveness that marked his quest for incunabula, becoming the most powerful Warlock of the Academy in less time than most practitioners required to complete a course of undergraduate work. Almost as an afterthought, he carved a staff of Dreadwood and summoned into it a captive spirit from the Outer Hells. And anticipating the day when he will have recovered every last lost spellbook, he has commenced writing his own Black Grimoire. It will undoubtedly be instructive.";
 			echo "</p>";
 			break;
 		case 'beastmaster':
 			echo "<header>";
 			echo "<h1>- Karroch -</h1>";
 			echo "<h2>The Beastmaster</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Karroch was born a child of the stocks. His mother died in childbirth; his father, a farrier for the Last King of Slom, was trampled to death when Karroch was five. Afterward Karroch was indentured to the king’s menagerie, where he grew up among all the beasts of the royal court: lions, apes, fell-deer, and things less known, things barely believed in. When the lad was seven, an explorer brought in a beast like none before seen. Dragged before the King in chains, the beast spoke, though its mouth moved not. Its words: a plea for freedom. The King only laughed and ordered the beast perform for his amusement; and when it refused, struck it with the Mad Scepter and ordered it dragged to the stocks. Over the coming months, the boy Karroch sneaked food and medicinal draughts to the wounded creature, but only managed to slow its deterioration. Wordlessly, the beast spoke to the boy, and over time their bond strengthened until the boy found he could hold up his end of a conversation—could in fact speak now to all the creatures of the King's menagerie. On the night the beast died, a rage came over the boy. He incited the animals of the court to rebel and threw open their cages to set them amok on the palace grounds. The Last King was mauled in the mayhem. In the chaos, one regal stag bowed to the boy who had freed him; and with Beastmaster astride him, leapt the high walls of the estate, and escaped. Now a man, Karroch the Beastmaster has not lost his ability to converse with wild creatures. He has grown into a warrior at one with nature’s savagery.";
 			echo "</p>";
 			break;
 		case 'queenofpain':
 			echo "<header>";
 			echo "<h1>- Akasha -</h1>";
 			echo "<h2>The Queen of Pain</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The Ecclesiast-King of Elze nursed a desire for pain — forbidden pain. In a less prominent political figure, such desires might be considered unwise, but in a monarch of his stature, to satisfy such thirsts would have threatened the virtue of the Divine Throne itself. Therefore he turned to his dungeon full of demonologists, promising freedom to whoever could summon a personal succubus of torment and bind it entirely to his service. The creature who arrived, Akasha by name, visited upon him such exquisite torments that he named her his Secret Queen, and he began to spend all his spare moments submitting to her clever torments—eventually abdicating all his responsibilities in his pursuit of the painful pleasures that only she could bring. Queen of Pain could bring him to the brink of death, but she was rune-bound to keep him alive. At last the King's neglect of state brought on an uprising. He was dragged from his chamber and hurled from the Tower of Invocations, and at the moment of death, Queen of Pain was let loose into the world, freed from servitude—freed to visit her sufferings on anyone she deigned to notice.";
 			echo "</p>";
 			break;
 		case 'venomancer':
 			echo "<header>";
 			echo "<h1>- Lesale Deathbringer -</h1>";
 			echo "<h2>The Venomancer</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In the Acid Jungles of Jidi Isle, poison runs in the veins and bubbles in the guts of every creature that scuttles, climbs or swoops between fluorescent vines dripping with caustic sap. Yet even in this toxic menagerie, Venomancer is acknowledged as the most venomous. Ages ago, a Herbalist named Lesale crossed the Bay of Fradj by coracle, searching for potent essences that might be extracted from bark and root, and found instead a nightmare transformation. Two leagues into Jidi's jungle, Lesale encountered a reptile camouflaged as an epiphyte, which stung him as he mistakenly plucked it. In desperation, he used his partial knowledge of the jungle's herbal bounty, mixing the venom of the (swiftly throttled) reptile with the nectar of an armored orchid, to compound an antidote. In the moments before a black paralysis claimed him completely, he injected himself by orchid-thorn, and instantly fell into a coma. Seventeen years later, something stirred in the spot where he had fallen, throwing off the years' accumulation of humus: Venomancer. Lesale the Herbalist no longer—but Lesale the Deathbringer. His mind was all but erased, and his flesh had been consumed and replaced by a new type of matter—one fusing the venom of the reptile with the poisonous integument of the orchid. Jidi's Acid Jungles knew a new master, one before whom even the most vicious predators soon learned to bow or burrow for their lives. The lurid isle proved too confining, and some human hunger deep in the heart of the Venomancer drove Lesale out in search of new poisons—and new deaths to bring.";
 			echo "</p>";
 			break;
 		case 'facelessvoid':
 			echo "<header>";
 			echo "<h1>- Darkterror -</h1>";
 			echo "<h2>The Faceless Void</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Darkterror the Faceless Void is a visitor from Claszureme, a realm outside of time. It remains a mystery why this being from another dimension believes the struggle for the Nemesis Stones is worth entering our physical plane, but apparently an upset in the balance of power in this world has repercussions in adjacent dimensions. Time means nothing to Darkterror, except as a way to thwart his foes and aid his allies. His long-view of the cosmos has given him a remote, disconnected quality, although in battle he is quite capable of making it personal.";
 			echo "</p>";
 			break;
 		case 'wraithking':
 			echo "<header>";
 			echo "<h1>- Ostarion -</h1>";
 			echo "<h2>The Wraith King</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "For untold years, King Ostarion built a kingdom from the remains of his enemies. It was an obsessive's errand, done to pass the long eternities of a monarchy that seemed fated never to end. He believed that as long as he built up the towers of his palace, he could not die. But eventually he learned that he had been deluded...that bone itself could perish. Deeply mistrustful of flesh, he sought a more permanent way of extending his reign, and at last settled on pursuit of wraith energy, a form of pure spirit given off by certain dark souls at death. Should he infuse himself with Wraith Essence, he thought he might create a body as luminous and eternal as his ego. On the millennial solstice known as Wraith-Night, he submitted to a rite of transformation, compelling his subjects to harvest enough souls to fuel his ambition for immortality. No one knows how many of his champions died, for the only survivor who mattered was the Wraith King who rose with the sun on the following morn. Now he rarely spends a moment on his glowing throne--but strides out with sword drawn, demanding a fealty that extends far beyond death.";
 			echo "</p>";
 			break;
 		case 'deathprophet':
 			echo "<header>";
 			echo "<h1>- Krobelus -</h1>";
 			echo "<h2>The Death Prophet</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Krobelus was a Death Prophet—which is one way of saying she told fortunes for the wealthiest of those who wished to look beyond the veil. But after years of inquiring on behalf of others, she began to seek clues on her own fate. When death refused to yield its secrets, she tried to buy them with her life. But the ultimate price proved insufficient. Death disgorged her again and again, always holding back its deepest mysteries. Her jealousy grew. Others could die for eternity—why not she? Why must she alone be cast back on the shores of life with such tiresome regularity? Why was she not worthy of the one thing all other living creatures took for granted? Still, she would not be discouraged. Each time she returned from the grave, she brought a bit of death back with her. Wraiths followed her like fragments of her shattered soul; her blood grew thin and ectoplasmic; the feasting creatures of twilight took her for their kin. She gave a little of her life with every demise, and it began to seem as if her end was in sight. With her dedication to death redoubled, and no client other than herself, Krobelus threw herself ever more fervently into death's abyss, intent on fulfilling the one prophecy that eluded her: That someday the Death Prophet would return from death no more.";
 			echo "</p>";
 			break;
 		case 'phantomassassin':
 			echo "<header>";
 			echo "<h1>- Mortred -</h1>";
 			echo "<h2>The Phantom Assassin</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Through a process of divination, children are selected for upbringing by the Sisters of the Veil, an order that considers assassination a sacred part of the natural order. The Veiled Sisters identify targets through meditation and oracular utterances. They accept no contracts, and never seem to pursue targets for political or mercenary reasons. Their killings bear no relation to any recognizable agenda, and can seem to be completely random: A figure of great power is no more likely to be eliminated than a peasant or a well digger. Whatever pattern the killings may contain, it is known only to them. They treat their victims as sacrifices, and death at their hand is considered an honor. Raised with no identity except that of their order, any Phantom Assassin can take the place of any other; their number is not known. Perhaps there are many, perhaps there are few. Nothing is known of what lies under the Phantom Veil. Except that this one, from time to time, when none are near enough to hear, is known to stir her veils with the forbidden whisper of her own name: Mortred.";
 			echo "</p>";
 			break;
 		case 'pugna':
 			echo "<header>";
 			echo "<h1>- Pugna -</h1>";
 			echo "<h2>The Oblivion</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In the realm of Pugna's birth, near the vents of the Nether Reaches, there stood a lamasery devoted to the Arts of Oblivion, which drew its power from the nether energies. The Grandmaster of the temple compound had himself passed into Oblivion several years prior, leaving his academy without a leader. From the moment of their master's death, the regents of the temple began rites of divination to identify their master's reincarnation, and eventually all signs converged on the immediate neighborhood. Several villages squatted in the shadow of the temple, their alleys and plazas full of the laughter of squalling children. Pugna, a mere thirteen months of age, was but one candidate among the local brats, and on the appointed day he was presented at the temple alongside two other promising tots. The lamas offered a jumble of worn relics to the children, treasured possessions of their former grandmaster. One boy reached for a porphyry wand that had belonged to the lama...and put it in his nostril. An impish girl pulled out an amulet that had also been the lama's, and immediately swallowed it. Pugna regarded the other two coolly, gave a merry laugh, and blasted them with gouts of emerald flame, reducing them to ashes in an instant. He then snatched up the wand and amulet, saying 'Mine!' The regents hoisted the beaming Pugna on their shoulders, wrapped him in their grandmaster's vestments, and rushed him to the throne before his mood could change. Within five years, the temple itself was another pile of ash, which pleased Pugna to no end.";
 			echo "</p>";
 			break;
 		case 'templarassassin':
 			echo "<header>";
 			echo "<h1>- Lanaya -</h1>";
 			echo "<h2>The Templar Assassin</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Lanaya, the Templar Assassin, came to her calling by a path of curious inquiry. Possessed of a scientific bent, she spent her early years engaged in meticulous study of nature's laws--peering into grimoires of magic and alchemy, recreating experiments from charred fragments of the Violet Archives, and memorizing observations of the Keen recordkeepers. Already quiet and secretive by nature, the difficulty of acquiring these objects further reinforced her skills of stealth. Had she been less retiring, she might have become notorious among the guilds as a thief-scholar. Instead her investigations led her into far more obscure corners. As she devoted her furtive talents to unlocking the secrets of the universe, she instead unlocked a secret door that exists in nature itself: the entryway to the most Hidden Temple. The intelligences that waited beyond that portal, proved to be expecting her, and whatever mysteries they revealed in the moment of their discovery was nothing compared to the answers they held out to Lanaya should she continue in their service. She swore to protect the mysteries, but more to the point, in service to the Hidden Temple she satisfies her endless craving for understanding. In the eyes of each foe she expunges, a bit more of the mystery is revealed.";
 			echo "</p>";
 			break;
 		case 'viper':
 			echo "<header>";
 			echo "<h1>- Viper -</h1>";
 			echo "<h2>The Netherdrake</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The malevolent familiar of a sadistic wizard who captured and hoped to tame him, Viper was curiously glad to have been sprung from the sealed and unchanging subterranean Nether Reaches where his race had lived for millions of years, after tectonic slippage had sealed off the Netherdrakes in luminous caverns. Viper spent some time appearing to submit to the wizard's enchainments, hoping to learn what he could of the dark magics the mage practiced. But he soon realized that few spells were as deadly as the toxins that were his birthright. Exuding an acid that swiftly ate away the bars of his cage, the Netherdrake slipped free of his confines, spit poison in the old spellcaster's eyes, and soared out to let the world know that it had a new master.";
 			echo "</p>";
 			break;
 		case 'luna':
 			echo "<header>";
 			echo "<h1>- Luna -</h1>";
 			echo "<h2>The Moon Rider</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "How had she been reduced to this? She was once the Scourge of the Plains, a merciless leader of men and beasts, and able to sow terror wherever she dared. Now she was far from her homeland, driven half mad from starvation and months of wandering, her army long dead or turned to worse. As she stood at the edge of an ancient forest, a pair of glowing eyes spied on from an elder branch. Something beautiful and deadly sought a meal in the wilting dusk. Without a sound, it turned and left. Fury overtook her. Clutching a rust-eaten dagger, she charged after the beast determined to reclaim even a shred of her past glory, but her quarry would not be caught. Three times she cornered the creature among the rocks and trees, and three times she pounced only to witness its fading shadow darting further into the woods. Yet the full moon shone brightly, and the creature’s trail was easy to follow. Arriving in a clearing atop a high hill, the beast’s massive feline form sat in the open, attentive and waiting. When the woman brandished her dagger, the creature reared and roared and charged. Death, it seemed, had come for her at long last in this strange place. She stood, calm and ready. A flash of movement, and the beast snatched the dagger from her hand before vanishing into the forest. Stillness. Hooded figures approached. In reverent tones they revealed that Selemene, Goddess of the Moon, had chosen her, had guided her, had tested her. Unwittingly she had endured the sacred rites of the Dark Moon, warriors of the Nightsilver Woods. She was offered a choice: join the Dark Moon and pledge herself to the service of Selemene, or leave and never return. She did not hesitate. Embracing her absolution, she renounced her bloody past, and took up a new mantle as Luna of the Dark Moon, the dreaded Moon Rider, ruthless and ever-loyal guardian of the Nightsilver Woods.";
 			echo "</p>";
 			break;
 		case 'dragonknight':
 			echo "<header>";
 			echo "<h1>- Davion -</h1>";
 			echo "<h2>The Dragon Knight</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "After years on the trail of a legendary Eldwurm, the skilled dragon-slayer found himself facing a disappointing foe: the dreaded Slyrak had grown ancient and frail, its wings tattered, its few remaining scales stricken with scale-rot, its fangs ground to nubs, and its fire-gouts no more threatening than a pack of wet matchsticks. Seeing no honor to be gained in dragon-murder, the young knight prepared to turn away and leave his old foe to die in peace. But a voice crept into his thoughts, and Slyrak gave a whispered plea that the knight might honor him with death in combat. The knight agreed, and found himself rewarded beyond expectation for his act of mercy: As he sank his blade in Slyrak's breast, the dragon sank a claw into his throat. As their blood mingled, Slyrak sent his power out along the Blood Route, offering all its strength and centuries of wisdom to the knight. The dragon's death sealed their bond, and Dragon Knight was born. The ancient power slumbers in the knight, waking when he calls it; or perhaps it is the Dragon that calls the Knight...";
 			echo "</p>";
 			break;
 		case 'dazzle':
 			echo "<header>";
 			echo "<h1>- Dazzle -</h1>";
 			echo "<h2>The Shadow Priest</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Each young acolyte to the Dezun order must complete a series of rites before becoming a shadow priest. The final rite, the rite of shades, is a harrowing spiritual journey through the Nothl Realm, an unpredictable domain from which not all visitants return. Of those who do, some return mad. Others return with strange aptitudes. But all who go there are changed by their experiences. Driven by the need for enlightenment, Dazzle was the youngest of his tribe ever to request the sacred ritual. At first the order refused him, saying he was too young. But Dazzle was not to be dissuaded. Sensing something special in the headstrong young acolyte, the elders relented. Dazzle drank down the sacred potion and sat by the fire while the rest of his tribe danced through the night. In this ethereal dimension of the Nothl Realm, the properties of light and dark are inverted. Thus his brilliant healing light, beautiful to our eye, is actually a sinister kind of evil; and the darkest deeds are done in a dazzling glow. The elders' intuition was prophetic: Dazzle returned to his people as a Shadow Priest like none seen before, with the power to heal as well as to destroy. Now he uses his gift to fight his enemies and help his friends.";
 			echo "</p>";
 			break;
 		case 'clockwerk':
 			echo "<header>";
 			echo "<h1>- Rattletrap -</h1>";
 			echo "<h2>The Clockwerk</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Rattletrap descends from the same far-flung kindred as Sniper and Tinker, and like many of the Keen Folk, has offset his diminutive stature through the application of gadgetry and wit. The son of the son of a clockmaker, Rattletrap was many years apprenticed to that trade before war rode down from the mountains and swept the plains villages free of such innocent vocations. “Your new trade is battle,” his dying father told him as the village of their ancestors lay in charred and smoking ruins. It is a poor tradesman who blames his tools, and Rattletrap was never one to make excuses. After burying his father among the ruins of their village, he set about to transform himself into the greatest tool of warfare that any world had ever seen. He vowed to never again be caught unprepared, instead using his talents to assemble a suit of powered Clockwerk armor to make the knights of other lands look like tin cans by comparison. Now Rattletrap is alive with devices—a small but deadly warrior whose skills at ambush and destruction have risen to near-automated levels of efficiency. An artisan of death, his mechanizations make short work of the unwary, heralding a new dawn in this age of warfare. What time is it? It's Clockwerk time!";
 			echo "</p>";
 			break;
 		case 'leshrac':
 			echo "<header>";
 			echo "<h1>- Leshrac -</h1>";
 			echo "<h2>The Tormented Soul</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Leshrac, Tormented Soul, is an entity torn from the heart of nature, a liminal being that exists half in one plane of existence, half in another. His penetrating intelligence is such that he can never ignore for a moment the agonizing horror at the heart of all creation. Once a great philosopher who sought the meaning of existence, he plumbed the depths of nature with the haunted Chronoptic Crystals, and was forever altered by the hideous mysteries thereby revealed to him. Now the darkest depths of his enlightenment are illumined only by the fitful glare of his arrogance. Like other elemental characters, he is completely at one with nature, but in his case it is a nature lurid and vile. He alone sees the evil truth of reality, and has no use for those who believe the cosmos reserves a special reward for those who practice benevolence.";
 			echo "</p>";
 			break;
 		case 'naturesprophet':
 			echo "<header>";
 			echo "<h1>-Furion-</h1>";
 			echo "<h2>The Nature's Prophet</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "When Verodicia, Goddess of the Woods, had finished filling in the green places, having planted the coiled-up spirit in the seed, having lured the twining waters from deep within the rock, having sworn the sun its full attention to the growing things, she realized that her own time had reached its end, and like one of the leaves whose fate she had imprinted in the seed, she would fall without seeing the fruiting of her dream. It pained her to leave the world bereft, for the sprouts had not yet broken through the soil--and they would be tender and vulnerable to every sort of harm. She found in her seed pouch one last seed that she had missed in the sowing. She spoke a single word into the seed and swallowed it as she fell. Her vast body decomposed throughout the long winter, becoming the humus that would feed the seedlings in the spring. And on the morning of the vernal equinox, before the rest of the forest had begun to wake, that last seed ripened and burst in an instant. From it stepped Nature's Prophet, in full leaf, strong and wise, possessing Verodicia's power to foresee where he would be needed most in defense of the green places--and any who might be fortunate enough to call him an ally.";
 			echo "</p>";
 			break;
 		case 'lifestealer':
 			echo "<header>";
 			echo "<h1>- N'aix -</h1>";
 			echo "<h2>The Lifestealer</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "In the dungeons of Devarque, a vengeful wizard lay in shackles, plotting his escape. He shared his cell with a gibbering creature known as N'aix, a thief cursed by the Vile Council with longevity, so that its life-sentence for theft and cozening might be as punishing as possible. Over the years, its chains had corroded, along with its sanity; N'aix retained no memory of its former life and no longer dreamt of escape. Seeing a perfect vessel for his plans, the wizard wove a spell of Infestation and cast his life-force into N'aix's body, intending to compel N'aix to sacrifice itself in a frenzy of violence while the mage returned to his body and crept away unnoticed. Instead, the wizard found his mind caught in a vortex of madness so powerful that it swept away his plans and shattered his will. Jarred to consciousness by the sudden infusion of fresh life, N'aix woke from its nightmare of madness and obeyed the disembodied voice that filled its skull, which had only the one thought: To escape. In that moment Lifestealer was born. The creature cast its mind into dungeon guards and soldiers, compelling them to open locks and cut down their companions, opening an unobstructed path to freedom while feeding on their lives. Lifestealer still wears the broken shackles as a warning that none may hold him, but on the inside remains a prisoner. Two minds inhabit the single form--a nameless creature of malevolent cunning, and the Master whose voice he pretends to obey.";
 			echo "</p>";
 			break;
 		case 'darkseer':
 			echo "<header>";
 			echo "<h1>- Ish'kafel -</h1>";
 			echo "<h2>The Dark Seer</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Fast when he needs to be, and a cunning strategist, Ish'Kafel the Dark Seer requires no edged weapons to vanquish his enemies, relying instead on the strength of his powerful mind. His talent lies in his ability to maneuver the fight to his advantage. Hailing from a place he calls 'The Land behind the wall,' Dark Seer remains an outsider here—a warrior from a realm beyond the veil of this reality. Once a great general among his people, and a valiant defender of the god-king Damathryx, Dark Seer’s army was wiped out by a much larger force in the final days of the Great Boundaries War. Facing certain defeat, he made one last desperate act: he led the enemy forces into the maze between the walls. At the last moment, just before capture, he crossed over—then sealed the walls forever in an explosive release of dark energy. When the dust settled, he saw that he had saved his people but found himself blinking at the sun of a different world, with no way to return. Now he is committed to proving his worth as a military strategist, and vows to show that he’s the greatest tactician this strange new world has ever seen.";
 			echo "</p>";
 			break;
 		case 'clinkz':
 			echo "<header>";
 			echo "<h1>- Clinkz -</h1>";
 			echo "<h2>The Bone Fletcher</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "At the base of the Bleeding Hills stretches a thousand-league wood—a place called The Hoven, where black pools gather the tarry blood of the uplands, and the king-mage Sutherex sits in benevolent rule. Once a sworn protector of the Hoven lands, Clinkz earned a reputation for his skill with a bow. In the three-hundredth year of the king-mage, the demon Maraxiform rose from sixth hell to lay claim to the forest. In response, the king-mage decreed an unbreakable spell: to any who slew the demon would be granted Life Without End. Unaware of the spell, Clinkz waded into battle, defending his lands against the demon’s fiery onslaught. Clinkz drove Maraxiform back to the gates of sixth-hell itself, where on that fiery threshold the two locked in a mortal conflict. Grievously wounded, the demon let out a blast of hellfire as Clinkz loosed his final arrow. The arrow struck the demon true as hellfire poured out across the land, lighting the black pools and burning Clinkz alive at the instant of the demon’s death. Thus, the mage’s spell took effect at the very moment of the archer’s conflagration, preserving him in this unholy state, leaving him a being of bones and rage, caught in the very act of dying, carrying hell’s breath with him on his journey into eternity.";
 			echo "</p>";
 			break;
 		case 'omniknight':
 			echo "<header>";
 			echo "<h1>- Purist Thunderwrath -</h1>";
 			echo "<h2>The Omniknight</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Purist Thunderwrath was a hard-fighting, road-worn, deeply committed knight, sworn to the order in which he had grown up as squire to elder knights of great reputation. He had spent his entire life in the service of the Omniscience, the All Seeing One. Theirs was a holy struggle, and so embedded was he in his duty that he never questioned it so long as he had the strength to fight and the impetuous valor that comes with youth. But over the long years of the crusade, as his elders passed away and were buried in sorry graves at the side of muddy tracks, as his bond-brothers fell in battle to uncouth creatures that refused to bow to the Omniscience, as his own squires were chewed away by ambush and plague and bad water, he began to question the meaning of his vows--the meaning of the whole crusade. After deep meditation, he parted ways with his army and commenced a long trek back to the cave-riddled cliffs of Emauracus, and there he set a challenge to the priests of the Omniscience. No knight had ever questioned them before, and they tried to throw him into the pit of sacrifice, but Purist would not be moved. For as he faced them down, he began to glow with a holy light, and they saw that the Omniscience had chosen to reveal Itself to him. The Elder Hierophant led him on a journey of weeks down into the deepest chamber, the holy of holies, where waited not some abstract concept of wisdom and insight, not some carved relic requiring an injection of imagination to believe in, but the old one itself. It had not merely dwelt in those rocks for billions of aeons; no, It had created them. The Omniscience had formed the immense mineral shell of the planet around itself, as a defense against the numerous terrors of space. Thus the All Seeing One claimed to have created the world, and given the other truths revealed to Purist on that day, the knight had no reason to refute the story. Perhaps the Omniscience is a liar, deep in its prison of stone, and not the world's creator at all, but Omniknight never again questioned his faith. His campaign had meaning at last. And there can be no question that the glorious powers that imbue him, and give his companions such strength in battle, are real beyond any doubt.";
 			echo "</p>";
 			break;
 		case 'enchantress':
 			echo "<header>";
 			echo "<h1>- Aiushtha -</h1>";
 			echo "<h2>The Enchantress</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Aiushtha appears to be an innocent, carefree creature of the woods, and while this is certainly true, it is hardly the sum of her story. She well understands the suffering of the natural world. She has wandered far, and fared through forests bright and drear, in every clime and every season, gathering friends, sharing news, bringing laughter and healing wherever she goes. For in worlds wracked by war, forests are leveled for the building of ships and siege engines; and even in places of peace, the woods are stripped for the building of homes, and as fuel for countless hearths. Aiushtha hears the pleas of the small creatures, the furtive folk who need green shade and a leafy canopy to thrive. She lends her ears to those who have no other listeners. She carries their stories from the wood to the world, believing that her own good cheer is a kind of Enchantment, that can itself fulfill the promise of a verdant future.";
 			echo "</p>";
 			break;
 		case 'huskar':
 			echo "<header>";
 			echo "<h1>- Huskar -</h1>";
 			echo "<h2>The Sacred Warrior</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Emerging from the throes of the sacred Nothl Realm, Huskar opened his eyes to see the prodigal shadow priest Dazzle working a deep incantation over him. Against the ancient rites of the Dezun Order, Huskar’s spirit had been saved from eternity, but like all who encounter the Nothl he found himself irrevocably changed. No longer at the mercy of a mortal body, his very lifeblood became a source of incredible power; every drop spilled was returned tenfold with a fierce, burning energy. However this newfound gift infuriated Huskar, for in his rescue from the Nothl, Dazzle had denied him a place among the gods. He had been denied his own holy sacrifice. In time the elders of the order sought to expand their influence and Huskar, they agreed, would be a formidable tool in their campaign. Yet becoming a mere weapon for the order that denied him his birthright only upset him further. As the first embers of war appeared on the horizon, he fled his ancestral home to find new allies, all the while seeking a cause worthy of unleashing the power his total sacrifice could bring.";
 			echo "</p>";
 			break;
 		case 'nightstalker':
 			echo "<header>";
 			echo "<h1>- Balanar -</h1>";
 			echo "<h2>The Night Stalker</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Of the Night Stalker, there is no history, only stories. There are ancient tales woven into the lore of every race and every culture, of an impossible time before sunlight and daytime, when night reigned alone and the world was covered with the creatures of darkness--creatures like Balanar the Night Stalker. It is said that on the dawn of the First Day, all the night creatures perished. All, that is, save one. Evil's embodiment, Night Stalker delights in his malevolence. He created the primal role of the Night Terror, the Boogeyman, and as long as there have been younglings, his is the specter summoned to terrify them. This is a role he relishes; nor are these empty theatrics. He does indeed stalk the unwary, the defenseless, those who have strayed beyond the lighted paths or denied the warnings of their communities. Night Stalker serves as living proof that every child's worst nightmare....is true.";
 			echo "</p>";
 			break;
 		case 'broodmother':
 			echo "<header>";
 			echo "<h1>- Black Arachnia -</h1>";
 			echo "<h2>The Broodmother</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "For centuries, Black Arachnia the Broodmother lurked in the dark lava tubes beneath the smoldering caldera of Mount Pyrotheos, raising millions of spiderlings in safety before sending them to find prey in the wide world above. In a later age, the Vizier of Greed, Ptholopthales, erected his lodestone ziggurat on the slopes of the dead volcano, knowing that any looters who sought his magnetic wealth must survive the spider-haunted passages. After millennia of maternal peace, Black Arachnia found herself beset by a steady trickle of furfeet and cutpurses, bold knights and noble youths--all of them delicious, certainly, and yet tending to create a less than nurturing environment for her innocent offspring. Tiring of the intrusions, she paid a visit to Ptholopthales; and when he proved unwilling to discuss a compromise, she wrapped the Vizier in silk and set him aside to be the centerpiece of a special birthday feast. Unfortunately, the absence of the Magnetic Ziggurat's master merely emboldened a new generation of intruders. When one of her newborns was trodden underfoot by a clumsy adventurer, she reached the end of her silken rope. Broodmother headed for the surface, declaring her intent to rid the world of each and every possible invader, down to the last Hero if necessary, until she could ensure her nursery might once more be a safe and wholesome environment for her precious spiderspawn.";
 			echo "</p>";
 			break;
 		case 'bountyhunter':
 			echo "<header>";
 			echo "<h1>- Gondar -</h1>";
 			echo "<h2>The Bounty Hunter</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "When the hunted tell tales of Gondar the Bounty Hunter, none are sure of which are true. In whispered tones they say he was abandoned as a kit, learning his skill in tracking as a matter of simple survival. Others hear he was an orphan of war, taken in by the great Soruq the Hunter to learn the master’s skill with a blade as they plumbed the dark forests for big game. Still others believe he was a lowly street urchin raised among a guild of cutpurses and thieves, trained in the arts of stealth and misdirection. Around campfires in the wild countryside his quarry speaks the rumors of Gondar’s work, growing ever more fearful: they say it was he who tracked down the tyrant King Goff years after the mad regent went into hiding, delivering his head and scepter as proof. That it was he who infiltrated the rebel camps at Highseat, finally bringing the legendary thief White Cape to be judged for his crimes. And that it was he who ended the career of Soruq the Hunter, condemned as a criminal for killing the Prince’s prized hellkite. The tales of Gondar’s incredible skill stretch on, with each daring feat more unbelievable than the last, each target more elusive. For the right price, the hunted know, anyone can be found. For the right price, even the mightiest may find fear in the shadows.";
 			echo "</p>";
 			break;
 		case 'weaver':
 			echo "<header>";
 			echo "<h1>- Skitskurr -</h1>";
 			echo "<h2>The Weaver</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The fabric of creation needs constant care, lest it grow tattered; for when it unravels, whole worlds come undone. It is the work of the Weavers to keep the fabric tight, to repair worn spots in the mesh of reality. They also defend from the things that gnaw and lay their eggs in frayed regions, whose young can quickly devour an entire universe if the Weavers let their attention lapse. Skitskurr was a master Weaver, charged with keeping one small patch of creation tightly woven and unfaded. But the job was not enough to satisfy. It nagged him that the original work of creation all lay in the past; the Loom had done its work and travelled on. He wanted to create rather than merely maintain—to weave worlds of his own devising. He began making small changes to his domain, but the thrill of creation proved addictive, and his strokes became bolder, pulling against the pattern that the Loom had woven. The guardians came, with their scissors, and Weaver's world was pared off, snipped from the cosmic tapestry, which they rewove without him in it. Skitskurr found himself alone, apart from his kind, a state that would have been torment for any other Weaver. But Skitskurr rejoiced, for now he was free. Free to create for himself, to begin anew. The raw materials he needed to weave a new reality were all around him. All he had to do was tear apart this old world at the seams.";
 			echo "</p>";
 			break;
 		case 'rubick':
 			echo "<header>";
 			echo "<h1>- Rubick -</h1>";
 			echo "<h2>The Grand Magus</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "Any mage can cast a spell or two, and a few may even study long enough to become a wizard, but only the most talented are allowed to be recognized as a Magus. Yet as with any sorcerer’s circle, a sense of community has never guaranteed competitive courtesy.
Already a renowned duelist and scholar of the grander world of sorcery, it had never occurred to Rubick that he might perhaps be Magus material until he was in the midst of his seventh assassination attempt. As he casually tossed the twelfth of a string of would-be killers from a high balcony, it dawned on him how utterly unimaginative the attempts on his life had become. Where once the interruption of a fingersnap or firehand might have put a cheerful spring in his step, it had all become so very predictable. He craved greater competition. Therefore, donning his combat mask, he did what any wizard seeking to ascend the ranks would do: he announced his intention to kill a Magus. Rubick quickly discovered that to threaten one Magus is to threaten them all, and they fell upon him in force. Each antagonist's spell was an unstoppable torrent of energy, and every attack a calculated killing blow. But very soon something occurred that Rubick's foes found unexpected: their arts appeared to turn against them. Inside the magic maelstrom, Rubick chuckled, subtly reading and replicating the powers of one in order to cast it against another, sowing chaos among those who had allied against him. Accusations of betrayal began to fly, and soon the sorcerers turned one upon another without suspecting who was behind their undoing. When the battle finally drew to a close, all were singed and frozen, soaked and cut and pierced. More than one lay dead by an ally’s craft. Rubick stood apart, sore but delighted in the week’s festivities. None had the strength to argue when he presented his petition of assumption to the Hidden Council, and the Insubstantial Eleven agreed as one to grant him the title of Grand Magus.";
 			echo "</p>";
 			break;
 		default:
 			echo "<header>";
 			echo "<h1>- Goblin Techies -</h1>";
 			echo "<h2>The Mortar Crew</h2>";
 			echo "<img src=\"img/hero/" . $heroName . ".png\">";
 			echo "</header>";
 			echo "<p>";
			echo "The monks of Turstarkuri watched the rugged valleys below their mountain monastery as wave after wave of invaders swept through the lower kingdoms. Ascetic and pragmatic, in their remote monastic eyrie they remained aloof from mundane strife, wrapped in meditation that knew no gods or elements of magic. Then came the Legion of the Dead God, crusaders with a sinister mandate to replace all local worship with their Unliving Lord's poisonous nihilosophy. From a landscape that had known nothing but blood and battle for a thousand years, they tore the souls and bones of countless fallen legions and pitched them against Turstarkuri. The monastery stood scarcely a fortnight against the assault, and the few monks who bothered to surface from their meditations believed the invaders were but demonic visions sent to distract them from meditation. They died where they sat on their silken cushions. Only one youth survived--a pilgrim who had come as an acolyte, seeking wisdom, but had yet to be admitted to the monastery. He watched in horror as the monks to whom he had served tea and nettles were first slaughtered, then raised to join the ranks of the Dead God's priesthood. With nothing but a few of Turstarkuri's prized dogmatic scrolls, he crept away to the comparative safety of other lands, swearing to obliterate not only the Dead God's magic users--but to put an end to magic altogether.";
 			echo "</p>";
 			break;
 	}
 	?>
 	<audio controls autoplay hidden>
	<source src="sound/<?php echo $heroName ?>.mp3" type="audio/mpeg">
	</audio>
 </body>
</html>