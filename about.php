<!DOCTYPE html>
<html>

  <head>
    <meta charset='UTF-8'>
    <title>About // Pubtra.in</title>
    <link rel="stylesheet" href="css/about.css" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton:300,400' rel='stylesheet' type='text/css'>
  </head>
   <body>
   <header>
    <h1>- Pubtra.in -</h1>
    <h2>About this site &amp; FAQS</h2>
   </header>
  <div>
    <p>This site was created as a proof of concept of data visualisation for the videogame <a href="http://www.dota2.com">Dota 2</a> as part of <a href="http://www.charlietizard.com">Charlie Tizard's</a> BSc.</p>
  </div>
  <div>
    <span class="question">What does this site do?</span>
    <span class="answer">This site creates a visualisation of the data from last 25 games of Dota 2 that you have played.</span>
  </div>
  
  <div>
    <span class="question">How often is the data updated?</span>
    <span class="answer">Currently your matches will be updated every two hours.</span>
  </div>
  
  <div>
    <span class="question">What is "gameplay focus"?</span>
    <span class="answer">Gameplay focus is an area of your gameplay that we suggest you focus on to improve your overall game.</span>
  </div>

  <div>
    <span class="question">My favourite hero isn't Goblin Techies!</span>
    <span class="answer">Firstly, you're wrong. Yes it is.<br /> Secondly, your favorite hero is the one you've played most in the last 25 matches!</span>
  </div>

  <div>
    <span class="question">Is this site run by Valve?</span>
    <span class="answer">No, this site has no connections to Valve Software.</span>
  </div>

  </body>
  <script type='text/javascript' src='js/canvasjs.min.js'></script>
</html>

<!-- /home/ajms/webapps/ct_pubtrain -->