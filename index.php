<?php

include "apikey.php";
// Stores my API key
include "openid.php";
// LightOpenID Library
include "herolist.php";
// Hero name array values set

////////////////////////////////////////////////////////////////////////////////////////////////////
// Based on LightOpenID tutorial https://www.youtube.com/watch?v=e2OnJfhkLxU                      //                                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////////

$OpenID = new LightOpenID("http://pubtra.in");
if (!isset($_SESSION)) { session_start(); }

if(!$OpenID->mode){

  if(isset($_GET['login'])){
   $OpenID->identity = "http://steamcommunity.com/openid";
   header("Location: {$OpenID->authUrl()}");
 }

 if(!isset($_SESSION['SteamAuth'])){
   $login = "<div id=\"login\">Please <a href=\"?login\"><img src=\"http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_small.png\"/> to begin.</div>";
 }

} elseif($OpenID->mode == "cancel"){
  echo "user has canceled authentication";

} else {
  if(!isset($_SESSION['SteamAuth'])){
   $_SESSION['SteamAuth'] = $OpenID->validate() ? $OpenID->identity : null;
   $_SESSION['Steam64'] = str_replace("http://steamcommunity.com/openid/id/", "", $_SESSION['SteamAuth']);

   if ($_SESSION['SteamAuth'] !== null) {

    $Steam64 = str_replace("http://steamcommunity.com/openid/id/", "", $_SESSION['SteamAuth']);
    $Steam32 = gmp_sub($Steam64, "76561197960265728");
    $profile = file_get_contents("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={$apikey}&steamids={$Steam64}");
    $matches = file_get_contents("http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key={$apikey}&account_id={$Steam64}");

    $buffer = fopen('data/' . $Steam64 . '.json', 'w+');
    fwrite($buffer, $profile);
    fclose($buffer);

    $buffer2 = fopen('data/' . $Steam64 . 'matches.json', 'w+');
    fwrite($buffer2, $matches);
    fclose($buffer2);

  }

  header("location: index.php");
}
}

if (isset($_SESSION['SteamAuth'])){

  $login = "<a href=\"?logout\">Logout</a>";
  $TEST = $_SESSION['SteamAuth'];

}

if (isset($_GET['logout'])){
  unset($_SESSION['SteamAuth']);
  unset($_SESSION['SteamID64']);
  header("Location: index.php");
}

////////////////////////////////////////////////////////////////////////////////////////////////////


if (isset($_SESSION['SteamAuth'])){
  $steam = json_decode(@file_get_contents("data/{$_SESSION['Steam64']}.json"));

  set_time_limit(120);

// $MatchHistory = User's GetMatchHistory
// $MatchID = List of User's MatchID's
// $SteamID64 = User's 64 bit SteamID
// $SteamID32 = User's 32 bit SteamID

  $SteamID64 = str_replace("http://steamcommunity.com/openid/id/", "", $_SESSION['SteamAuth']);
  $MatchData = array();
  $PlayerSlot = array();

//Defines the user's 64 bit SteamID
  $SteamID32 = gmp_sub($SteamID64, '76561197960265728');
  $SteamID32 = gmp_strval($SteamID32);

  if (file_exists('data/' . $SteamID32 . '.json')) {
  // If file exists, don't make an API call.
    if (time()-filemtime('data/' . $SteamID32 . '.json') > 2 * 3600) {
    // if older than 2 hours
      unlink('data/' . $SteamID32 . '.json');
      // delete match history
      $MatchHistory = file_get_contents('http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=' . $apikey . '&account_id=' . $SteamID32 . '.json');
      // 
      file_put_contents('data/' . $SteamID32 . '.json', $MatchHistory);
      // Saves the match history as STEAMID.json 
    } else {
      // file younger than 2 hours
      $MatchHistory = file_get_contents('data/' . $SteamID32 . '.json');

    }
  }
  else {
// Else make an API call to get the user's match history
    $MatchHistory = file_get_contents('http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/V001/?key=' . $apikey . '&account_id=' . $SteamID32 . '.json');
// 
    file_put_contents('data/' . $SteamID32 . '.json', $MatchHistory);
// Saves the match history as STEAMID.json 
  }

  $MatchHistory = json_decode(file_get_contents('data/' . $SteamID32 . '.json'));
// Decodes json file into PHP Array

  for ($i=0; $i<=24; $i++)
  {
    $MatchID[$i] = $MatchHistory->result->matches[$i]->match_id;
    $MatchDate[$i] = gmdate('d-m-Y', $MatchHistory->result->matches[$i]->start_time);
    // Converts UNIX Timestamp into GMT D/M/Y
  }
// Loops though each match and adds the match_id to the $MatchID

  for ($i=0; $i<=24; $i++)
  {
    if (file_exists('data/' . $MatchID[$i] . '.json')) {
      // If the match data is already downloaded

    }
    else {
      $MatchData = file_get_contents('http://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key=' . $apikey . '&match_id=' . $MatchID[$i]);
      // Make an API call
      file_put_contents('data/' . $MatchID[$i] . '.json', $MatchData);
      // Saves the match history as MATCHID.json 
    }
  }
// Loops though a users matches, downloads any missing match data.

  $players = array();
  $count = 0;
  $denycount = 0; 
  $lasthitcount = 0;
  $deathcount = 0;

  for ($i=0; $i<=24; $i++){

    $MatchData = file_get_contents('data/' . $MatchID[$count] . '.json');
    // Get the contents of a match and store it in $match
    $MatchData = json_decode($MatchData);
    // Decodes json file into an array

    foreach($MatchData->result->players as $player){
      @$players[$player->account_id] = $player;
    }

    $LastHits[$i] = $players[$SteamID32]->last_hits;
      // Stores the user's last hits into an array.

    if ($LastHits[$i] <= 100) {
      $lasthitcount++;
    }
    // counts the games the player scored less than 100 last hits

    $Denies[$i] = $players[$SteamID32]->denies;
      // Stores the user's denies into an array.

    if ($Denies[$i] <= 10) {
      $denycount++;
    }
    // counts the games the player had less than ten denies

    $GoldPerMin[$i] = $players[$SteamID32]->gold_per_min;
      // Stores the user's gold per minute into an array.
    $ExpPerMin[$i] = $players[$SteamID32]->xp_per_min;
      // Stores the user's experiance per minute into an array.
    $HeroID[$i] = $players[$SteamID32]->hero_id;
      // Stores the user's hero into an array.  
    $Kills[$i] = $players[$SteamID32]->kills;
      // Stores the user's kills into an array.  
    $Deaths[$i] = $players[$SteamID32]->deaths;
      // Stores the user's deaths into an array.

    if ($Deaths[$i] >= 10) {
      $deathcount++;
    }
    // counts the games the player scored died less than or equal to 10 times

    $Assists[$i] = $players[$SteamID32]->assists;
      // Stores the user's assists into an array. 
    $count++;
    
  } 

  if ($denycount >= 10){
    $gameplayFocus = "Denies";
    $gameplayFocusURL = "https://www.youtube.com/watch?v=ru1wYo-GmfQ?rel=0&vq=large";
  }

  if ($lasthitcount >= 10) {
   $gameplayFocus = "Last Hits";
   $gameplayFocusURL = "https://www.youtube.com/watch?v=ru1wYo-GmfQ?rel=0&vq=large";
 }

 if ($deathcount >= 10) {
  $gameplayFocus = "Mechanics";
  $gameplayFocusURL = "https://www.youtube.com/watch?v=ru1wYo-GmfQ?rel=0&vq=large";
}

$heroArr = array();

foreach ($HeroID as $hero) {
  if (!isset($heroArr[$hero])) {
    $heroArr[$hero] = 1;
  } else {
    $heroArr[$hero]++;
  }

}
arsort($heroArr);
$favHero = current(array_keys($heroArr));

$removeURL = array();
$removeURL[] = " ";
$removeURL[] = '&#8217;';
$favHeroURL = str_replace($removeURL, "", $HeroName[$favHero]);


?>

<!DOCTYPE html>
<html>

<head>
  <meta charset='UTF-8'>
  <title>Profile // Pubtra.in</title>
  <link rel="stylesheet" href="css/main.css" type="text/css">
  <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
  <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
  <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton:300,400' rel='stylesheet' type='text/css'>
  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
  <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
  <script type='text/javascript' src='js/canvasjs.min.js'></script>

  <script type='text/javascript'>   
   $(document).ready(function() {
    $(".various").fancybox({
      maxWidth  : 800,
      maxHeight : 600,
      fitToView : false,
      width   : '70%',
      height    : '70%',
      autoSize  : false,
      closeClick  : false,
      openEffect  : 'none',
      closeEffect : 'none',
      padding : 2

    });

    $('.fancybox-media').fancybox({
      openEffect  : 'none',
      closeEffect : 'none',
      helpers : {
        media : {}
      }
    });

  });
 </script>

 <script type='text/javascript'>   
  window.onload = function () {
    var chart = new CanvasJS.Chart('LastHits', {
      animationEnabled: true,
      backgroundColor: "rgb(0,0,0,0)",
      title:{
        text: "Last Hits",
        fontColor: "#A7A7A7"             
      },
      data: [              
      { 
       type: 'column',
       color: "#369ead",
       dataPoints: [
       <?php

       for ($i=24; $i >= 0; $i--) {
        echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $LastHits[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Last Hits: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
      }

      ?>
      ]
    }
    ]
  });

    chart.render();

    var chart = new CanvasJS.Chart('Denies', {
      animationEnabled: true,
      backgroundColor: "rgb(0,0,0,0)",
      title:{
        text: 'Denies',
        fontColor: "#A7A7A7"               
      },
      data: [             
      { 
       type: 'column',
       color: "#369ead",
       dataPoints: [
       <?php

       for ($i=24; $i >= 0; $i--) {
        echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $Denies[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Denies: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
      }

      ?>
      ]
    }
    ]
  });

    chart.render();

    var chart = new CanvasJS.Chart('GoldPerMin', {
      animationEnabled: true,
      backgroundColor: "rgb(0,0,0,0)",
      title:{
        text: 'Gold Per Minute',
        fontColor: "#A7A7A7"               
      },
      data: [         
      {

       type: 'line',
       color: "#369ead",
       dataPoints: [
       <?php

       for ($i=24; $i >= 0; $i--) {
        echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $GoldPerMin[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Gold Per Minute: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
      }

      ?>
      ]
    }
    ]
  });

    chart.render();

    var chart = new CanvasJS.Chart('ExpPerMin', {
      animationEnabled: true,
      backgroundColor: "rgb(0,0,0,0)",
      title:{
        text: 'Experience Per Minute',
        fontColor: "#A7A7A7"               
      },
      data: [          
      { 

       type: "line",
       color: "#369ead",
       dataPoints: [
       <?php

       for ($i=24; $i >= 0; $i--) {
        echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $ExpPerMin[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Experience Per Minute: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
      }

      ?>
      ]
    }
    ]
  });

    chart.render();


    var chart = new CanvasJS.Chart("com", {
      animationEnabled: true,
      backgroundColor: "rgb(0,0,0,0)",
      title:{
        text: "KDA - Kills/Deaths/Assists",
        fontColor: "#A7A7A7" 

      },   
      data: [{        
        type: "column",
        color: "#7cb62f",
        dataPoints: [
        <?php

        for ($i=24; $i >= 0; $i--) {
          echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $Kills[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Kills: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
        }

        ?>
        ]

      },
      {        
        type: "column",
        color: "#b52f2f",
        dataPoints: [
        <?php

        for ($i=24; $i >= 0; $i--) {
          echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $Deaths[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Deaths: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
        }

        ?>
        ]
      },
      {        
        type: "column",
        color: "#369ead",
        dataPoints: [
        <?php

        for ($i=24; $i >= 0; $i--) {
          echo '{ label: \'' . $MatchDate[$i] . '\', y: ' . $Assists[$i] . ', toolTipContent: "<b>Hero: </b><img class=\'heroicon\' src=\'img/icons/' . $HeroIcon[$HeroID[$i]] .'\' />' . $HeroName[$HeroID[$i]] . '<br/><b>Assists: </b> {y} <br/><b>MatchID: </b><a href=\'http://dotabuff.com/matches/' . $MatchID[$i] . '\'>' . $MatchID[$i] . '</a>"},';
        }

        ?>
        ]
      }         
      ]
    });


chart.render();

} 
</script>
</head>
<body>
 <header>
  <nav>
    <ul>
      <li><a id="home" href="index.php">Profile</a></li>
      <li><a id="about" class="various" data-fancybox-type="iframe" href="about.php">About</a></li>
      <li><a id="logout" href="?logout">Logout</a></li>
    </ul>
  </nav>
</header>
<div id="profile">
  <div id="profileinfo">
    <p><a href="<?php echo @$steam->response->players[0]->profileurl;  ?>"><?php echo @$steam->response->players[0]->personaname; ?></a></p>

    <?php echo @"<img src=\"{$steam->response->players[0]->avatarfull}\"/>";  ?>
  </div><div id="profileoptions"> 
  <ul>
    <li>Gameplay Focus: <a class="fancybox-media" href="<?php echo $gameplayFocusURL ?>"><?php echo $gameplayFocus ?></a></li>
    <li>Favoured Hero: <a class="various" data-fancybox-type="iframe" href=<?php echo '"lore.php?name=' . strtolower($favHeroURL) . '">' . $HeroName[$favHero] ?></a></li>
    <li>Games Won: 15/25</li>
  </ul>
</div>
</div>
<div></div>    
<div id='LastHits' class="graph" style='height: 300px; width: 100%;'></div>
<div id='Denies' class="graph" style='height: 300px; width: 100%;'></div>
<div id='GoldPerMin' class="graph" style='height: 300px; width: 100%;'></div>
<div id='ExpPerMin' class="graph" style='height: 300px; width: 100%;'></div>
<div id='com' class="graph" style='height: 300px; width: 100%;'></div>
<footer>
  <hr/>
  <p>
    Pubtra.in
  </p>
</footer>
<?php
print_r($Steam64);
?>
</body>
</html>
<?php

}

else {
  ?>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <title>Dota 2 // Pubtra.in</title>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton' rel='stylesheet' type='text/css'>
    <title>Profile // Pubtra.in</title>
    <link rel="stylesheet" href="css/main.css" type="text/css">
    <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Neuton:300,400' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type='text/javascript' src='js/canvasjs.min.js'></script>
    <script type='text/javascript'>   
     $(document).ready(function() {
      $(".various").fancybox({
        maxWidth  : 800,
        maxHeight : 600,
        fitToView : false,
        width   : '70%',
        height    : '70%',
        autoSize  : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
      });
    });
   </script>  
 </head>
 <body>
  <header>
    <nav>
      <ul>
        <li><a id="home" href="index.php">Profile</a></li>
        <li><a id="about" class="various" data-fancybox-type="iframe" href="about.php">About</a></li>
        <li><a id="logout" href="?logout">Logout</a></li>
      </ul>
    </nav>
  </header>
  <?php echo "<div id=\"loginimage" . rand(1,4) . "\">"; ?></div>
  <div id="login">
    <p>Welcome to Pubtra.in, please login to continue!</p>
    <a href="?login"><img src="http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_small.png"/></a>
  </div>
  <footer>
    <hr/>
    <p>
      Pubtra.in
    </p>
  </footer>
</body>
</html>

<?php }

?>